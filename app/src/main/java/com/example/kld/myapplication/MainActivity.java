package com.example.kld.myapplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

public class MainActivity extends ActionBarActivity {

    DrawerLayout mDrawerlayout;
    ActionBarDrawerToggle mDrawerToggle;
    ImageButton imgRightMenu;
    ExpandableListAdapter mDrawerList_Right;
    ExpandableListView expendListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    private int position;
    public static final int SET = Menu.FIRST;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //===============Initialization of Variables=========================//

        mDrawerlayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        imgRightMenu = (ImageButton) findViewById(R.id.imgRightMenu);
        mDrawerlayout.setDrawerListener(mDrawerToggle);



        expendListView = (ExpandableListView) findViewById(R.id.drawer_list_right);

        prepareListData();
        mDrawerList_Right = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        expendListView.setAdapter(mDrawerList_Right);
        expendListView.setOnItemClickListener(new DrawerItemClickListener());

        //============== Define a Custom Header for Navigation drawer=================//

        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.navigation_header, null);

        imgRightMenu = (ImageButton) v.findViewById(R.id.imgRightMenu);

        getSupportActionBar().setHomeButtonEnabled(true);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayUseLogoEnabled(false);

        getSupportActionBar().setDisplayShowCustomEnabled(true);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1281A9")));

        getSupportActionBar().setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        getSupportActionBar().setCustomView(v);


        imgRightMenu.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (mDrawerlayout.isDrawerOpen(expendListView)){
                    mDrawerlayout.closeDrawer(expendListView);
                }
                else {
                    mDrawerlayout.openDrawer(expendListView);
                }
            }
        });




    }


    // create menu for the test of Setting Activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0,SET,0,"setting");
        return super.onCreateOptionsMenu(menu);
    }

    // click menu event
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case 1:
                Intent mIntent = new Intent();
                mIntent.setClass(this, SettingActivity.class);
                startActivity(mIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // click listener
    private class DrawerItemClickListener implements ExpandableListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }


    private void selectItem(int position) {
        expendListView.setItemChecked(position,true);
//        if(position==0) {
//            Intent mIntent = new Intent();
//            mIntent.setClass(this, SettingActivity.class);
//            startActivity(mIntent);
//        }
        switch(position){
            case 0:
                startActivity(new Intent(this,SettingActivity.class));
                break;
            default:
                break;
        }

    }



    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Setting");

        listDataHeader.add("Top 250");
        listDataHeader.add("Now Showing");
        listDataHeader.add("Coming Soon..");

        List<String> setting = new ArrayList<String>();

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");
        top250.add("The Good, the Bad and the Ugly");
        top250.add("The Dark Knight");
        top250.add("12 Angry Men");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");

        listDataChild.put(listDataHeader.get(1), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(2), nowShowing);
        listDataChild.put(listDataHeader.get(3), comingSoon);

        listDataChild.put(listDataHeader.get(0), setting);
    }
}
